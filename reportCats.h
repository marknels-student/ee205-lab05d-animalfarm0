///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// Configuration header file for the Report Cats module
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern void   printCat( const size_t index ) ;
extern void   printAllCats() ;
extern size_t findCat( const char* name ) ;  /// @returns the index or BAD_CAT
