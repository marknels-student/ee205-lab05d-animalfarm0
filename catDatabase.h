///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// Configuration header file for the cat database in Animal Farm
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>  // For the bool datatype
#include <stddef.h>   // For the size_t datatype

#include "config.h"

#define MAX_CAT_NAME (30)   /// This defines the size of the cat's name string

enum Gender { UNKNOWN_GENDER=0, MALE, FEMALE } ;
enum Breed { UNKNOWN_BREED=0, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };

extern char        catName   [MAX_CATS][MAX_CAT_NAME] ;
extern enum Gender catGender [MAX_CATS] ;
extern enum Breed  catBreed  [MAX_CATS] ;
extern bool        catIsFixed[MAX_CATS] ;
extern float       catWeight [MAX_CATS] ;

extern size_t numCats ;  /// This points to the next available cat we can add
                         /// to the database.

extern void initializeDatabase() ;

extern bool isFull() ;   /// Return true if the database is full

extern bool validateDatabase();  /// Verify that the database is healthy

extern bool isIndexValid( const size_t index );  /// Verify the index is a valid value for this database

extern bool isNameValid( const char* name );  /// Verify the name is valid

extern bool isWeightValid( const float weight );  /// Verify the weight is valid

extern void wipeCat( const size_t index );  /// Zeros out a cat's information from the database

extern bool swapCat( const size_t a, const size_t b );  /// Swap the cats in the indexes
