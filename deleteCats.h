///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.h
/// @version 1.0
///
/// Configuration header file for the Delete Cats module
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern bool deleteAllCats() ;
extern bool deleteCat( const size_t index );