///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file config.h
/// @version 1.0
///
/// Configuration header file for Animal Farm
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <limits.h>

#define PROGRAM_TITLE "Animal Farm 0"
#define PROGRAM_NAME  "animalfarm0"

#define MAX_CATS (128)   /// This is the maximum number of cats the database
                         /// can hold.

#define BAD_CAT UINT_MAX
