///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// Configuration header file for the Add Cats module
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "catDatabase.h"

extern int addCat( const char* name
                  ,const enum Gender gender
                  ,const enum Breed breed
                  ,const bool isFixed
                  ,const float weight ) ;
