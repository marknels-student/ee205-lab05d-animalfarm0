///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// This modules finds and prints cats in the catabase.
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>  // For strncmp

#include "reportCats.h"
#include "catDatabase.h"

void printCat( const size_t index ) {
	if( !isIndexValid( index ) ) {
		fprintf( stderr, "%s: %s(): Bad cat!\n", PROGRAM_NAME, __FUNCTION__ ) ;
		return;
	}

	printf( "cat index = [%lu]  name=[%s]  gender=[%d]  breed=[%d]  isFixed=[%d]  weight=[%f]\n", index, catName[index], catGender[index], catBreed[index], catIsFixed[index], catWeight[index] ) ;
}


void printAllCats() {
	#ifdef DEBUG
		printf( "numCats = [%lu]\n", numCats );
	#endif

	for( size_t i = 0 ; i < numCats ; i++ ) {
		printCat( i );
	}
}


size_t findCat( const char* name ) {
	if( name == NULL ) {
		return BAD_CAT ;  // Silently return... no cat found
	}

	for( size_t i = 0 ; i < numCats ; i++ ) {
		if( strncmp( name, catName[i], MAX_CAT_NAME ) == 0 ) {  // Found a match!
			return i;
		}
	}

	return BAD_CAT ; // No name matched
}
