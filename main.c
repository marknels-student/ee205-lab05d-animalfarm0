///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// Orchestrates the entire program.  Start by printing
///    Starting Animal Farm 0
/// ...and end by printing
///    Done with Animal Farm 0
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "config.h"
#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"


int main() {
	printf( "Starting %s\n", PROGRAM_TITLE );

	initializeDatabase();

	addCat( "Loki",  MALE,           PERSIAN,    true,   8.5 ) ;
	addCat( "Milo",  MALE,           MANX,       true,   7.0 ) ;
	addCat( "Bella", FEMALE,         MAINE_COON, true,  18.2 ) ;
	addCat( "Kali",  FEMALE,         SHORTHAIR,  false,  9.2 ) ;
	addCat( "Trin",  FEMALE,         MANX,       true,  12.2 ) ;
	addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR,  false, 19.0 ) ;

	#ifdef DEBUG
		// Test for empty name
		addCat( "", UNKNOWN_GENDER, SHORTHAIR,  false, 19.0 ) ;
		// Test for max name
		addCat( "12345678901234567890123456789", UNKNOWN_GENDER, SHORTHAIR,  false, 19.0 ) ;
		// Test for name too long
		addCat( "123456789012345678901234567890", UNKNOWN_GENDER, SHORTHAIR,  false, 19.0 ) ;
		// Test for duplicate cat name
		addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR,  false, 0 ) ;
		// Test for weight <= 0
		addCat( "Neo", UNKNOWN_GENDER, SHORTHAIR,  false, 0 ) ;

		// Test for printCat( -1 );
		printCat( -1 );
		// Test for out of bounds
		printCat( 100 );

		// Test finding a cat...
		assert( findCat( "Bella" ) == 2 ) ;
		// Test not finding a cat
		assert( findCat( "Bella's not here" ) == BAD_CAT ) ;

		// Test setting a large name
		size_t testCat = addCat( "Oscar", UNKNOWN_GENDER, SHORTHAIR,  false, 1.1 ) ;
		updateCatName( testCat, "12345678901234567890123456789" );
		printCat( testCat );

		// Test setting an out-of-bounds name
		updateCatName( testCat, "123456789012345678901234567890" );
		printCat( testCat );

		// Test setting an illegal cat weight
		updateCatWeight( testCat, -1 );

	#endif

	printAllCats();

	int kali = findCat( "Kali" ) ;
	updateCatName( kali, "Chili" ) ; // this should fail
	printCat( kali );
	updateCatName( kali, "Capulet" ) ;
	updateCatWeight( kali, 9.9 ) ;
	fixCat( kali ) ;
	printCat( kali );

	printAllCats();

	deleteAllCats();
	printAllCats();

	printf( "Done with %s\n", PROGRAM_TITLE );

	return( EXIT_SUCCESS );
}
