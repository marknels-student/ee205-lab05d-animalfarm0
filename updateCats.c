///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// This modules updates cats in the catabase.
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>  // For memset and strncpy

#include "updateCats.h"

bool updateCatName( const size_t index, const char* name ) {
	if( !isIndexValid( index ) ) {
		fprintf( stderr, "%s: %s(): Bad cat!\n", PROGRAM_NAME, __FUNCTION__ ) ;
		return false;
	}

	if( !isNameValid( name )) {
		return false;
	}

	// Do the deed
	memset( catName[index], 0, MAX_CAT_NAME ) ;  // Zero out the old cat's name
	strncpy( catName[index], name, MAX_CAT_NAME-1 );

	return true;
}


bool fixCat( const size_t index ) {
	if( !isIndexValid( index ) ) {
		fprintf( stderr, "%s: %s(): Bad cat!\n", PROGRAM_NAME, __FUNCTION__ ) ;
		return false;
	}

	// Do the deed
	catIsFixed[index] = true;

	return true;
}


bool updateCatWeight( const size_t index, const float weight ) {
	if( !isIndexValid( index ) ) {
		fprintf( stderr, "%s: %s(): Bad cat!\n", PROGRAM_NAME, __FUNCTION__ ) ;
		return false;
	}

	if( !isWeightValid( weight)) {
		return false;
	}

	// Do the deed
	catWeight[index] = weight;

	return true;
}
