///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Aminal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// Configuration header file for the Update Cats module
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @date   07_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>

#include "catDatabase.h"


/// All of these functions return true if they succeed
extern bool updateCatName( const size_t index, const char* name ) ;
extern bool fixCat( const size_t index ) ;
extern bool updateCatWeight( const size_t index, const float weight ) ;
